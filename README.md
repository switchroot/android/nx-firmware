# NX Firmware for Android

## Sources

nx HOS stock dump:
- CYW4356A3_001.004.009.0092.0095.bin (BT patchfile)
- brcmfmac4356A3-pcie.txt (WiFi NVRAM, includes ccode mod)

mdarcy Android stock dump:
- brcmfmac4356-pcie.clm_blob (CLM data)

custom built:
-  bl33.bin from https://github.com/makinbacon21/switch-l4t-uboot/tree/v10_03_2024